[
  "if" "ifx" "return" "else" "for" "while" "using" "struct" "enum"
  "then" "defer" "case" "continue" "break"
  "remove" "xx" "enum_flags"
  ; "push_context" "operator" "cast"
] @keyword

[
  (inline_comment)
  (block_comment)
] @comment


(builtin_type) @type
(type_expression (identifier) @type)

(
  (identifier) @constant
  (#match? @constant "^[A-Z][A-Z_0-9]+$")
)


[
  (binary_operator)  
  (unary_operator)
  ".."
] @operator

[
  (string_literal)
] @string

[
  (int_literal)
  (float_literal)
  (scientific_notation)
  (uninitialized)
  (boolean_literal)
  "null"
] @constant.numeric

[
  ":" "::" "=" "->"
] @operator.important.jai


(function_decl function_name: (identifier) @function.decl.jai)
(struct_decl name: (identifier) @function.decl.jai)
(enum_decl name: (identifier) @function.decl.jai)

(import_expression "#import" @keyword.directive.jai )
[
  "#modify" "#scope_file" "#scope_module" "#scope_export" "#if"
  "#load" "#module_parameters" "#foreign" "#system_library"
  "#library" "#assert" "#no_abc" "#run" "#bake_arguments" "#location"
  "#filepath" "#through" "inline"
  (random_directive)
] @keyword.directive.jai

(function_call name: (identifier) @function)

[
  "if"
  "ifx"
  "return"
  "else"
  "for"
  "while"
  "using"
  "struct"
  "enum"
  ; "cast"
  "then"
  ; "defer"
  ; "case"
  "continue"
  "break"
  ; "push_context"
  ; "operator"
  "remove"
  "xx"
] @keyword

[
  (inline_comment)
  (block_comment)
] @comment


[
  (builtin_type)
  (type_expression)
] @type

(
  (identifier) @constant
  (#match? @constant "^[A-Z][A-Z_0-9]+$")
)


[
  (binary_operator)  
  (unary_operator)
] @operator

[
  (string_literal)
] @string

[
  (number)
  (float_literal)
  (scientific_notation)
  (uninitialized_token)
] @constant.numeric

[
  ":" "::" "=" ":=" "->" "=>"
] @operator.important.jai


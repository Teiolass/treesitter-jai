module.exports = grammar({
  name: 'jai',

  extras: $ => [
    /\s/,
    $.inline_comment,
    $.block_comment,
    $.random_directive,
    // 'using',
  ],

  rules: {
    source_file: $ => repeat(
      $.statement,
    ),

    statement: $ => prec(10, choice(
      seq($.variable_decl, ';'),
      seq($.constant_decl, ';'),
      $.function_decl,
      $.struct_decl,
      $.enum_decl,
      seq($.assignement, ';'),

      seq($.expression, ';'),

      $.for_loop,
      $.while_loop,
      $.if_statement,
      $.block,
      $.scope,
      $.load,
      $.module_parameter,
      $.foreign_function,
      $.assert,
      $.defer,
      $.switch,
      $.case,
    )),

    expression: $ => prec.left(0, choice(
      $.int_literal,
      $.float_literal,
      $.scientific_notation,
      $.string_literal,
      $.identifier,
      $.binary_expression,
      $.unary_expression,
      $.import_expression,
      $.function_call,
      $.block,
      $.uninitialized,
      $.remove_expression,
      seq('(', $.expression, ')'),
      $.loop_control,
      $.if_statement,
      $.return,
      $.array_literal,
      $.array_access,
      $.boolean_literal,
      'null',
      $.directive_expression,
      $.enum_literal,
      $.struct_literal,
      '#through',
    )),

    enum_literal: $ => seq(
      '.', $.identifier,
    ),

    struct_literal: $ => seq(
      optional($.identifier),
      '.',
      '{',
      optional(seq(
        $.identifier,
        '=',
        $.expression
      )),
      repeat(seq(
        ',',
        $.identifier,
        '=',
        $.expression
      )),
      optional(','),
      '}',
    ),

    case: $ => seq(
      'case',
      $.expression,
      ';'
    ),

    switch: $ => prec(3, seq(
      'if',
      $.expression,
      '==',
      $.block
    )),

    defer: $ => seq(
      'defer',
      $.statement,
      ';'
    ),

    assert: $ => prec.left(seq(
      '#assert',
      $.expression,
      optional($.string_literal)
    )),

    foreign_function: $ => seq(
      optional('using'),
      $.identifier,
      '::',
      $.arguments_decl,
      optional(seq('->', $.type_expression)),
      '#foreign',
      $.identifier,
      ';'
    ),

    boolean_literal: $ => choice(
      'true', 'false'
    ),

    inline_comment: $ => token(seq('//', /.*/)),
    block_comment: $ => seq(
      '/*',
      repeat(choice(/./, $.block_comment)),
      '*/'
    ),

    variable_decl: $ => prec.left(seq(
      optional('using'),
      repeat('$'),
      field('name', $.identifier),
      ':',
      field('type', optional($.type_expression)),
      optional(
        seq(
          '=',
          $.expression,
        ),
      ),
    )),

    constant_decl: $ => choice(
        seq(
          optional('using'),
          field('name', $.identifier),
          ':',
          field('type', optional($.type_expression)),
          ':',
          $.expression,
        ),
        seq(
          optional('using'),
          field('name', $.identifier),
          '::',
          $.expression,
        ),
    ),

    function_decl: $ => seq(
      field('function_name', $.identifier),
      '::',
      optional('inline'),
      $.arguments_decl,
      optional(
        seq(
          '->',
          $.type_expression,
        ),
      ),
      optional(seq(
        '#modify',
        $.block    
      )),
      $.block
    ),

    arguments_decl: $ => seq(
      '(',
      optional($.variable_decl),
      repeat(seq(
        ',',
        $.variable_decl,
      )),
      optional(','),
      ')',
    ),

    block: $ => seq(
      '{',
      repeat(
        $.statement,
      ),
      '}',
    ),

    type_expression: $ => choice(
      $.type_unary,
      $.builtin_type,
      $.identifier,
      $.struct_decl,
      $.enum_decl,
    ),

    binary_expression: $ => prec.left(seq(
      $.expression,
      $.binary_operator,
      $.expression,
    )),

    binary_operator: $ => choice(
      '+', '-', '*', '/', '>', '<',
      '==', '>=', '<=', '>>', '<<',
      '&&', '||', '%', '&', '^', '|',
      '<<<', '>>>', '/interface',
      '#align', '!=',
    ),

    unary_expression: $ => prec.right(seq(
      $.unary_operator,
      $.expression,
    )),

    unary_operator: $ => prec(1, choice(
      '!', '*', '<<', '-', '~', '$', 'xx'
    )),

    type_unary: $ => seq(
      $.type_unary_op,
      $.type_expression,
    ),

    type_unary_op: $ => choice(
      '*',
      '[..]',
      '[]',
      seq('[', $.int_literal, ']'),
      '$',
    ),

    builtin_type: $ => choice(
      'int',
      'float',
      'bool',
      'string',
      's8',
      's16',
      's32',
      's64',
      'u8',
      'u16',
      'u32',
      'u64',
      'float32',
      'float64',
      'Any',
      'void',
    ),

    import_expression: $ => seq(
      '#import',
      $.string_literal,
    ),

    function_call: $ => prec(1, seq(
      field('name', $.identifier),
      '(',
      optional($.expression),
      repeat(prec.left(seq(
        optional(seq($.identifier, '=')),
        ',',
        $.expression
      ))),
      optional(','),
      ')'
    )),

    uninitialized: $ => '---',
    
    for_loop: $ => seq(
      'for',
      repeat(choice('*', '<')),
      optional(seq(
        field('iterator_var', $.identifier),
        repeat(seq(',', field('iterator_var', $.identifier))),
        ':',
      )),
      choice($.expression, $.range),
      $.statement,
    ),

    range: $ => prec.left(3, seq(
      $.expression,
      '..',
      prec(100,$.expression),
    )),

    loop_control: $ => prec.right(seq(
      choice('break', 'continue'),
      optional($.identifier),
    )),

    if_statement: $ => prec.right(seq(
      choice('if', '#if', 'ifx'),
      $.expression,
      optional('then'),
      choice($.statement, $.expression),
      optional(seq(
        'else',
        choice($.statement, $.expression),
      ))
    )),


    remove_expression: $ => seq(
      'remove',
      $.identifier,
    ),

    
    string_literal: $ => seq(
      '"',
      repeat(choice(
        token.immediate(prec(1, /[^\\"\n]+/)),
        $.escape_sequence
      )),
      '"',
    ),

    struct_decl: $ => seq(
      field('name', $.identifier),
      '::',
      'struct',
      optional($.arguments_decl),
      $.block,
    ),

    enum_decl: $ => seq(
      field('name', $.identifier),
      '::',
      choice('enum', 'enum_flags'),
      optional($.type_expression),
      $.block,
    ),

    assignement: $ => seq(
      $.assign_lhs,
      $.assignement_operator,
      $.expression,
    ),

    assignement_operator: $ => choice(
      '=', '+=', '-=', '*=', '/=', '|=', '&=',
      '^=', '%='
    ),

    assign_lhs: $ => prec.left(-2, seq(
      $.identifier,
      repeat(seq(
        ',',
        $.identifier,
      ))
    )),

    return: $ => prec.left(seq(
      'return',
      optional(seq(
        $.expression,
        repeat(prec.left(seq(',', $.expression))
      )))
    )),

    while_loop: $ => seq(
      'while',
      choice($.variable_decl, $.expression),
      $.statement
    ),

    scope: $ => choice(
      '#scope_file',
      '#scope_module',
      '#scope_export',
    ),

    array_literal: $ => seq(
      optional($.identifier),
      '.', '[',
      optional($.expression),
      repeat(prec.left(seq(',', $.expression))),
      optional(','),
      ']'
    ),

    array_access: $ => seq(
      $.identifier,
      '[',
      $.expression,
      ']'
    ),

    random_directive: $ => choice(
      '#run', '#bake_arguments', '#assert', '#system_library', '#library',
      '#no_abc', '#foreign', '#compiler', '#c_call', '#no_abc', '#as',
    ),

    directive_expression: $ => choice(
      "#location", "#filepath"
    ),

    load: $ => seq(
      '#load',
      optional(seq(',', choice(
        'dir', 'string'
      ))),
      ';'
    ),

    module_parameter: $ => seq(
      '#module_parameters',
      $.arguments_decl,
    ),

    escape_sequence: $ => token(prec(1, seq(
      '\\',
      choice(
        /[^xuU]/,
        /\d{2,3}/,
        /x[0-9a-fA-F]{2,}/,
        /u[0-9a-fA-F]{4}/,
        /U[0-9a-fA-F]{8}/
      )
    ))),

    identifier: $ => /[a-zA-Z_][a-zA-Z_0-9\\]*/,

    int_literal: $ => /\d[\d_]*|0(h|x|X)[a-fA-F0-9_]+|0b[01_]+/,
    float_literal: $ =>/\d[\d_]*\.\d[\d_]*|\.\d[\d_]*/,

    scientific_notation: $ => token(seq(
      /\d[\d_]*\.\d+|\d[\d_]*|\.\d[\d_]*|0(h|x|X)[a-fA-F0-9_]+|0b[01_]+/,
      choice("e", "E"),
      optional(choice("+","-")),
      /\d[\d_]*\.\d+|\d[\d_]*|\.\d[\d_]*|0(h|x|X)[a-fA-F0-9_]+|0b[01_]+/,
    ))
  }
})

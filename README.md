# Jai Treesitter grammar

This is just a quick and dirty grammar for Jai. I am aware there is [this repository](https://github.com/Pyromuffin/tree-sitter-jai) on github, but it seems outdated not not working 100%. Queries in particular are very incomplete. My version does not express the whole jai grammar (which is quite articulated, especially with all the directives), but does its job in recogninzing what to highlight.

Built with [Helix editor](https://helix-editor.com/) in mind.

Contributions would be super appreciated.
